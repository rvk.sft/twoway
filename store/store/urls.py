from django.contrib import admin
from django.urls import path, include

from rest_framework.routers import DefaultRouter
from items.views import StoreOrderViewSet


router = DefaultRouter()
router.register(r'api', StoreOrderViewSet, basename='orders')


urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'', include(router.urls)),
]
