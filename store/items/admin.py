from django.contrib import admin
from django.contrib import auth

from items.models import StoreOrder


class StoreOrderAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        obj.from_admin_site = True
        super().save_model(request, obj, form, change)


admin.site.register(StoreOrder, StoreOrderAdmin)

admin.site.unregister(auth.models.User)
admin.site.unregister(auth.models.Group)
