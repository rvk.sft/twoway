import requests

from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver

from warehouse import settings


NEW = "New"
IN_PROCESS = "In Process"
STORED = "Stored"
SENT = "Sent"

STATUSES = (
    (NEW, "New"),
    (IN_PROCESS, "In Process"),
    (STORED, "Stored"),
    (SENT, "Sent"),
)


class WarehouseOrder(models.Model):
    order_number = models.CharField(max_length=10, unique=True, blank=False, null=False)
    status = models.CharField(choices=STATUSES, default=None, max_length=10)

    def __str__(self):
        return 'WAREHOUSE ORDER: {} - {}'.format(self.order_number, self.status)

    @property
    def hash(self):
        return hash(str(self.order_number) + str(self.status))


@receiver(pre_save, sender=WarehouseOrder)
def api_call(sender, instance, **kwargs):
    from_admin_site = getattr(instance, 'from_admin_site', False)
    if (from_admin_site):
        data = {
            "order_number": instance.order_number,
            "status": instance.status
        }
        headers = {'Authorization': 'Token ' + settings.STORE_TOKEN}
        response = requests.post(settings.SYNC_URL + 'api/order/', data=data, headers=headers)
