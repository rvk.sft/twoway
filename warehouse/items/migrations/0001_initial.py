# Generated by Django 3.1.7 on 2021-02-24 19:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='WarehouseOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_number', models.CharField(max_length=10)),
                ('status', models.CharField(choices=[('New', 'New'), ('In Process', 'In Process'), ('Stored', 'Stored'), ('Sent', 'Sent')], default=None, max_length=10)),
            ],
        ),
    ]
