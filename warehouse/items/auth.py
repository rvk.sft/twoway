from rest_framework import authentication
from rest_framework import exceptions

from django.conf import settings


class StoreAuthentication(authentication.BaseAuthentication):
    authentication_header_prefix = 'Token'

    def authenticate_header(self, request):
        """
        Return a string to be used as the value of the `WWW-Authenticate`
        header in a `401 Unauthenticated` response, or `None` if the
        authentication scheme should return `403 Permission Denied` responses.
        """
        return self.authentication_header_prefix

    def authenticate(self, request):
        """
        The `authenticate` method is called on every request regardless of
        whether the endpoint requires authentication.

        `authenticate` has two possible return values:

        1) `None` - We return `None` if we do not wish to authenticate. Usually
                    this means we know authentication will fail. An example of
                    this is when the request does not include a token in the
                    headers.

        2) `(user, token)` - We return a user/token combination when
                             authentication is successful.

                            If neither case is met, that means there's an error
                            and we do not return anything.
                            We simple raise the `AuthenticationFailed`
                            exception and let Django REST Framework
                            handle the rest.
        """

        # `auth_header` should be an array with two elements: 1) the name of
        # the authentication header (in this case, "Bearer") and 2) the token
        # that we should authenticate against.
        auth_header = authentication.get_authorization_header(request).split()
        if not auth_header:
            return None
        if len(auth_header) == 1:
            # Invalid token header. No credentials provided. Do not attempt to
            # authenticate.
            return None
        elif len(auth_header) > 2:
            # Invalid token header. The Token string should not contain spaces.
            # Do not attempt to authenticate.
            return None

        # Decode bytes to strings
        prefix = auth_header[0].decode('utf-8')
        token = auth_header[1].decode('utf-8')

        if prefix.lower() != self.authentication_header_prefix.lower():
            # The auth header prefix is not what we expected. Do not attempt to authenticate.
            return None

        # By now, we are sure there is a *chance* that authentication will
        # succeed. We delegate the actual credentials authentication to the
        # method below.
        return self._authenticate_credentials(request, token)

    def _authenticate_credentials(self, request, token):
        """
        Try to authenticate the given credentials. If authentication is
        successful, return the True and token. If not, throw an error.
        """
        if token == settings.STORE_TOKEN:
            return (True, token)
        else:
            raise exceptions.AuthenticationFailed('Invalid token')
