from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from django.core.exceptions import ObjectDoesNotExist

from .models import WarehouseOrder
from .auth import StoreAuthentication
from .permissions import IsStoreAuthenticated


class WarehouseOrderViewSet(viewsets.ModelViewSet):
    authentication_classes = (StoreAuthentication, )
    permission_classes = (IsStoreAuthenticated, )

    @action(detail=False, methods=['post'])
    def order(self, request):
        try:
            order_number = request.data.get('order_number')
            order_status = request.data.get('status')
            order, created = WarehouseOrder.objects.get_or_create(
                order_number=order_number,
                defaults={'order_number': order_number, 'status': order_status}
            )

            if not created:
                order.status = request.data.get('status')
                order.save()
            else:
                return Response(status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)

    @action(detail=False, methods=['delete'])
    def delete(self, request):
        try:
            order_number = request.data.get('order_number')
            order = WarehouseOrder.objects.get(order_number=order_number)
            order.delete()
        except ObjectDoesNotExist:
            return Response({'error': 'There is no order with this order number'}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)
